<?php
	require "partials/header.php";
	//var_dump($_SESSION);
	//die();
?>

<!-- CART -->
<div class="container">
	<div class="row">
		<div class="col">
			<table class="table">
				<thead>
				    <tr>
				      <th scope="col">Name</th>
				      <th scope="col">Price</th>
				      <th scope="col">Action</th>
				    </tr>
				</thead>
				<tbody>
					<?php 
						for($i = 0; $i<count($products); $i++){
					?>
				    <tr>
						<th scope="row"><?php echo $products[$i]["name"]; ?></th>
						<td><?php echo $products[$i]["price"]; ?></td>
						<td>
							<a href="product.php?productid=<?php echo $i; ?>">
								<button type="button" class="btn btn-primary">View</button>
							</a>
							<button type="button" class="btn btn-warning">Edit</button>
							<button type="button" class="btn btn-danger">Delete</button>
						</td>
				    </tr>
				    <?php } ?>
				</tbody>
			</table>
		</div>
	</div>

</div>

<?php
	require "partials/footer.php";
?>

