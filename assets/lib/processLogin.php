<?php
	session_start();
	$email = $_POST["email"];
	$password = $_POST["password"];

	var_dump($email);
	var_dump($password);

	//GET CONTENTS OF USERS.JSON
	$users_objects = file_get_contents("users.json");

	//CONVERT ABOVE INTO PHP ARRAY
	$users = json_decode($users_objects, true);

	for($i = 0; $i < count($users); $i++){
		if($users[$i]["email"] == $email && $users[$i]["password"] == $password){
			$_SESSION["email"] = $email;
			$_SESSION["isAdmin"] = $users[$i]["isAdmin"];
			$_SESSION["message"] = "Welcome, {$users[$i]['firstname']}";
			if($users[$i]["isAdmin"]){
				
				header("Location: ../../products.php");

				exit();
			} else {
				header("Location: ../../index.php");
				exit();
			}
		} else {
			$_SESSION["error_message"] = "Wrong credentials!";
			header("Location: ../../login.php");
		}
	}
?>